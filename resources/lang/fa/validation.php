<?php

return [
    'required' => 'پر کردن :attribute الزامی است',
    'unique' => ':attribute نمی تواند تکراری باشد',
    'max' => [
        'numeric' => ':attribute نباید بزرگتر از :max .باشد',
        'string' => 'طول :attribute بیش تر از :max کاراکتر است.',
    ],
    'string' => ':attribute نمی تواند عددی باشد',

    'attributes' => [
        'title' => 'عنوان',
        'icon' => 'آیکون',
        'select-icon' => 'انتخاب آیکون',
        'insert' => 'درج',
        'select-category' => 'انتخاب مجموعه',
        'description' => 'توضیحات',
        'quantity' => 'موجودی (عدد)',
        'price' => 'قیمت (ریال)',
        'discount' => 'تخفیف',
        'status' => 'وضعیت',
        'active' => 'فعال',
        'de-active' => 'غیر فعال',
        'category' => 'دسته بندی',
        'operation' => 'عملیات',
        'edit' => 'ویرایش',
        'delete' => 'حذف',
        'add-new-product' => 'ایجاد محصول جدید',
        'add-new-category' => 'ایجاد دسته جدید',
        'level' => 'سطح دسترسی',
        'admin' => 'مدیر',
        'user' => 'کاربر',
        'category_id' => 'دسته بندی',
        'first_name' => 'نام',
        'last_name' => 'نام‌خانوادگی',
        'user_name' => 'نام‌کاربری',
        'password' => 'رمز‌عبور',
        'email' => 'ایمیل',
        'phone_number' => 'موبایل',
    ]
];
