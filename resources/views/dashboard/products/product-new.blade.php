@extends('dashboard.master')

@section('content')
    <style>
        .box {
            width: 500px;
            height: auto;
            background-color: #e3e3e3;
            margin: 100px auto;
            border-radius: 10px;
        }

        .in-box {
            padding: 15px;
        }

        form label {
            float: right;
        }

    </style>
    <div class="box">
        <div class="in-box form-group">

            <form action="{{url('admin/products')}}" method="post" enctype="multipart/form-data">
                @method('POST')

                <select class="form-control" name="category_id" id="">
                    <option value="" selected>{{__('validation.attributes.select-category')}}</option>
                    @foreach($category as $item)
                        <option value="{{$item->id}}">{{$item->title}}</option>
                    @endforeach
                </select>
                @error('category_id')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <label for="title">{{__('validation.attributes.title')}}</label>
                <input class="form-control" type="text" name="title" id="title" value="{{ old('title') }}">
                @error('title')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <label for="description">{{__('validation.attributes.description')}}</label>
                <textarea class="form-control" name="description" id="description" cols="30" rows="3">{{ old('description') }}</textarea>
                @error('description')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <div style="float: right">
                    <label for="files" class="btn-sm btn-info">{{__('validation.attributes.select-icon')}}</label>
                    <input id="files" style="visibility:hidden;" type="file" name="icon">
                </div>
                @error('icon')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br><br>

                <label for="quantity">{{__('validation.attributes.quantity')}}</label>
                <input class="form-control" type="text" name="quantity" id="quantity" value="{{ old('quantity') }}">
                @error('quantity')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <label for="price">{{__('validation.attributes.price')}}</label>
                <input class="form-control" type="text" name="price" id="price" value="{{ old('price') }}">
                @error('price')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <label for="discount">{{__('validation.attributes.discount')}}</label>
                <input class="form-control" type="text" name="discount" id="discount" value="{{ old('discount') }}">
                @error('discount')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <select class="form-control" name="status" id="">
                    <option value="">{{__('validation.attributes.status')}}</option>
                    <option value="1" @if( old('status') == '1' ) selected="selected" @endif>{{__('validation.attributes.active')}}</option>
                    <option value="0" @if( old('status') == '0' ) selected="selected" @endif>{{__('validation.attributes.de-active')}}</option>
                </select>
                @error('status')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <button type="submit" class="form-control btn btn-success">{{__('validation.attributes.insert')}}</button>
            </form>
        </div>
    </div>

@endsection
