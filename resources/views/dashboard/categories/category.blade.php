@extends('dashboard.master')

@section('sidebar')
    @parent
@endsection

@section('content')
    <div class="container marketing">

    <div  style="margin: 15px">
        <a class="btn btn-primary" href="{{route('category.create')}}">{{__('validation.attributes.add-new-category')}}</a>
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>{{__('validation.attributes.title')}}</th>
            <th>{{__('validation.attributes.icon')}}</th>
            <th>{{__('validation.attributes.operation')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($category as $key=>$item)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$item->title}}</td>
                <td>
                    <img id="preview" src="{{asset('storage/'.$item->icon)}}" style="width:50px;height:50px;">
                </td>
                <td>{{$item->operations}}
                    <div class="btn-group">
                            <span><form method="GET" action="{{route('category.edit',['category_id'=>$item->id])}}">
                                    <button class="btn btn-link">ویرایش </button>
                                </form></span>
                        |
                        <span><a href="#" data-target="#exampleModal" data-toggle="modal" class="btn btn-link"
                                 style="color: red" data-id={{$item->id}}>حذف</a></span>
                        <span><form id="delete-user" method="POST" action="{{ route('category.delete',$item->id) }}">
                        @csrf
                                @method('delete')

                                </form>
                        </span>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @include('dashboard.pagation_default', ['paginator' => $category])
    <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">حذف کاربر</h5>

                    </div>
                    <div class="modal-body">
                        مطمئنی میخوای حذف کنی؟
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">خیر</button>
                        <button type="submit" form="delete-user" class="btn btn-primary">بله</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>

    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/jquery.ui.custom.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.uniform.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/matrix.js')}}"></script>
    <script src="{{asset('js/matrix.tables.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script>
        $(".deleteRecord").click(function () {
            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
            }, function () {
                window.location.href = "/admin/" + deleteFunction + "/" + id;
            });
        });
    </script>
@endsection



