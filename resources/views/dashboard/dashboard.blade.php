@extends('dashboard.master')
@section('content')
        <div class="container" style=" padding: 30px;">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                <h1 class="h5"><a href="{{route("products.show")}}">آخرین محصولات اضافه شده</a></h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group ml-2">
                        <button class="btn btn-sm btn-outline-secondary"><a href="{{route('products.show')}}">بیشتر</a></button>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($productGroup as $product)
                    <div class="col-12 col-md-3">
                        <div class="card mb-4 box-shadow">
                            <img class="card-img-top"
                                 src="{{asset('storage/'.$product->icon)}}"
                                 alt="Card image cap" width="140" height="140">
                            <div class="card-body">

                                <h5><a
                                       href=href="{{route('product.edit',['product_id'=>$product->id])}}"
                                       role="button">{{$product->title}}</a></h5>
                                <div class="d-flex justify-content-between align-items-center">
                                    <small class="text-muted">{{$product->price.'تومان'}}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            <hr>
        </div>

@endsection
