@extends('layouts.app')


@section('content')


<div class="container" dir="rtl" >
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="text-align: right;">{{ __('fields.dashboard') }}</div>

                <div class="card-body" style="text-align: right;">

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('fields.you-are-logged-in') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
