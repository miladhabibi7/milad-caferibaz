@extends('dashboard.master')

@section('sidebar')
    @parent
@endsection

@section('content')

    <div class="container marketing">
        <div>
            <table class="table table-striped">
                <thead class="col">
                <tr>
                    <th>#</th>
                    <th>شماره سفارش</th>
                    <th>سفارش دهنده</th>
                    <th>تاریخ سفارش</th>
                    <th>عملیات</th>

                </tr>
                </thead>
                <tbody>
                @foreach($orders as $item)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$item->order_number}}</td>
                        <td>{{$item->getUser->first_name}}</td>
                        <td>{{$item->created_at->toDateString()}}</td>
{{--                        <td>{{$item->getCategory->title}}</td>--}}
{{--                        <td>--}}
{{--                            <img id="preview" src="{{asset('storage/'.$item->icon)}}" style="width:50px;height:50px;">--}}
{{--                        </td>--}}
{{--                        <td>{{$item->quantity}}</td>--}}
{{--                        <td>{{$item->price}}</td>--}}
{{--                        <td>{{$item->discount}}</td>--}}
{{--                        <td>{{$item->status}}</td>--}}
                        <td>{{$item->operations}}
                            <div class="btn-group">
                            <span><form method="GET" action="{{route('product.edit',['product_id'=>$item->id])}}">
                                    <button class="btn btn-link">ویرایش </button>
                                </form></span>
                                |
                                <span><form method="POST" action="{{ route('product.delete',$item->id) }}">
                        @csrf
                                        @method('delete')
                        <button class="btn btn-link" style="color: red">حذف</button>
                                </form>
                        </span>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
{{--        @include('dashboard.pagation_default', ['paginator' => $orders])--}}
    </div>
@endsection


