@extends('frontend.mainlayout')
@section('content' )
    <style>
        .box {
            width: 500px;
            height: auto;
            background-color: #e3e3e3;
            margin: 20px auto;
            border-radius: 10px;
        }

        .in-box {
            padding: 15px;
            background-color: #343a40;
            color:silver;

        }

        .card {
            width:200px;
            margin-right:36%;
            border: 2px solid #343a40;
        }


        form label {
            float: right;
        }
    </style>
<div class="container"   dir="rtl"  style="text-align: right;">
    <div class="row justify-content-center" >
        <div class="col-md-8" >
            <div>
                <br><br>
            </div>
            <div class="card">
                <div class="card-header">
                    <h5 style="text-align: center">ثبت نام</h5>
                </div>
            </div>
            <div class="box">
                <div class="in-box form-group">

                    <form method="POST" action="{{ route('register') }}">
                        @csrf


                            <label for="first_name" class="col-md-4 col-form-label text-md-right" >{{ __('fields.first-name') }}</label>
                                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus>
                                @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror


                            <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ __('fields.last-name') }}</label>
                                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>
                                @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror


                        <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{ __('fields.phone-number') }}</label>
                        <input id="phone_number" type="tel" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number" autofocus>
                        @error('phone_number')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror

                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('fields.e-mail-address') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror


                            <label for="user_name" class="col-md-4 col-form-label text-md-right">{{ __('fields.user-name') }}</label>
                                <input id="user_number" type="text" class="form-control @error('user_name') is-invalid @enderror" name="user_name" value="{{ old('user_name') }}" required autocomplete="user_name" autofocus>
                                @error('user_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror





                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('fields.password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror





                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('fields.confirm-password') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">






                            <br>
                        <div style="text-align: left">
                            <button type="submit" class="btn btn-primary" >
                                {{ __('fields.register') }}
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


