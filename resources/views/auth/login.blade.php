@extends('frontend.mainlayout')
@section('content' )
    <style>
        .box {
            width: 500px;
            height: auto;
            background-color: #e3e3e3;
            margin: 20px auto;
            border-radius: 10px;
        }

        .in-box {
            padding: 15px;
            background-color: #343a40;
            color:silver;

        }

        .card {
            width:200px;
            margin-right:36%;
            border: 2px solid #343a40;
        }

        form label {
            float: right;
        }
    </style>

    <div class="container" dir="rtl" style="text-align: right;">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div>
                    <br><br>
                </div>
                <div class=" card">
                    <div class=" card-header">
                        <h5 style="text-align: center">خوش آمدید!</h5>
                    </div>
                </div>
                <div class="box">
                    <div class="in-box form-group">

                        <form method="POST" action="{{ route('login') }}">
                            @csrf


                            <label for="email"
                                   class="col-md-4 col-form-label text-md-right">{{ __('fields.e-mail-address') }}</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                   name="email" value="{{ old('email') }}" required autocomplete="email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror


                            <label for="password"
                                   class="col-md-4 col-form-label text-md-right">{{ __('fields.password') }}</label>
                            <input id="password" type="password"
                                   class="form-control @error('password') is-invalid @enderror" name="password" required
                                   autocomplete="new-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            <br>

                            <div class="container">
                                <label class="form-check-label" for="remember" style="text-align: right">
                                    {{ __('fields.remember-me') }}
                                </label>
                                <input type="checkbox" name="remember"
                                       id="remember" {{ old('remember') ? 'checked' : '' }}>
                            </div>

                            <br>

                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    رمز عبور خود را فراموش کرده اید؟
                                </a>
                            @endif
                            <br>

                            <a class="btn btn-link" href="{{ route('register') }}">
                                عضو نیستید؟ برای ثبت نام کلید کنید
                            </a>

                            <div style="text-align: left">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('fields.login') }}
                                </button>
                                <br>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



