@extends('frontend.mainlayout')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div>
                <br><br>
            </div>
            <div class="container marketing" style="margin: 20px">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    افزودن آدرس جدید
                </button>
            </div>
            <div class="card mx-4">
                <div class="card-body p-4">
                    <div class="card-header">
                        <h5 style="text-align: center">انتخاب آدرس</h5>
                    </div>
                    <br>
                    <table class="table table-striped">
                        <thead class="col">
                        <tr>
                            <th>#</th>
                            <th>عنوان</th>
                            <th>آدرس</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($addresses as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->title}}</td>
                                <td>{{$item->address}}</td>
                                <td><span><a class="btn btn-primary"
                                             href="{{route('order',['address_id'=>$item->id])}}">انتخاب</a></span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">آدرس خود را وارد کنید.</h5>
{{--                    <button  type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                        <span aria-hidden="true">&times;</span>--}}
                    </button>
                </div>
                <div class="modal-body">
                    <form id="add-address-form" action="{{route('save_address')}}" method="post">
                        @method('POST')
                        @csrf
                        <br>
                        <label for="title">عنوان آدرس</label>
                        <input class="form-control" type="text" name="title" id="title" placeholder="یک عنوان برای آدرس خود وارد کنید" value="{{ old('title') }}">
                        @error('title')
                        <div style="color:#cc0000">
                            {{$message}}
                        </div>
                        @enderror
                        <br>

                        <label for="address">آدرس</label>
                        <textarea class="form-control" name="address" id="address" cols="30" rows="3">{{ old('address') }}</textarea>
                        @error('address')
                        <div style="color:#cc0000">
                            {{$message}}
                        </div>
                        @enderror
                        <br><br>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" form="add-address-form" data-dismiss="modal" class="btn btn btn-secondary">انصراف</button>
                    <button  type="submit" form="add-address-form" class="btn btn-primary">تایید</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script>
            $(document).ready(function () {
                $('select[name]')
                $('#state_id').change(function () {
                    $('#city_id').html('<option value="" selected>Choose city</option>');
                    $('#city').show(150);

                    var state_id = $(this).val();
                    var $city = $('#city_id');

                    $.ajax({
                        url: "{{ route('cities')}}",
                        type: 'GET',
                        data: {
                            state_id: state_id
                        },
                        success: function (data) {
                            console.log(data);
                            $city.html('<option value="" selected>Choose city</option>');
                            $.each(data, function (id, value) {
                                $city.append('<option value="' + id + '">' + value + '</option>');
                            });
                            $('#city').show(150);
                        }
                    });
                });

            });
        </script>
{{--    <script src="{{asset('js/jquery.min.js')}}"></script>--}}
{{--    <script src="{{asset('js/jquery.ui.custom.js')}}"></script>--}}
{{--    <script src="{{asset('js/bootstrap.min.js')}}"></script>--}}
{{--    <script src="{{asset('js/jquery.uniform.js')}}"></script>--}}
{{--    <script src="{{asset('js/select2.min.js')}}"></script>--}}
{{--    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>--}}
{{--    <script src="{{asset('js/matrix.js')}}"></script>--}}
{{--    <script src="{{asset('js/matrix.tables.js')}}"></script>--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>--}}
{{--    <script>--}}
{{--        $(".deleteRecord").click(function () {--}}
{{--            var id = $(this).attr('rel');--}}
{{--            var deleteFunction = $(this).attr('rel1');--}}
{{--            swal({--}}
{{--                title: 'Are you sure?',--}}
{{--                text: "You won't be able to revert this!",--}}
{{--                type: 'warning',--}}
{{--                showCancelButton: true,--}}
{{--                confirmButtonColor: '#3085d6',--}}
{{--                cancelButtonColor: '#d33',--}}
{{--                confirmButtonText: 'Yes, delete it!',--}}
{{--                cancelButtonText: 'No, cancel!',--}}
{{--                confirmButtonClass: 'btn btn-success',--}}
{{--                cancelButtonClass: 'btn btn-danger',--}}
{{--                buttonsStyling: false,--}}
{{--                reverseButtons: true--}}
{{--            }, function () {--}}
{{--                window.location.href = "/admin/" + deleteFunction + "/" + id;--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}
@endsection

