@extends('frontend.mainlayout')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div>
                <br><br>
            </div>
            <div class="card mx-4">
                <div class="card-body p-4">
                    <div class="card-header">
                        <h5 style="text-align: center">انتخاب استان و شهر</h5>
                    </div>
                    <br>
                    <form action="{{url('updatecity/'.$user_id->id)}}" method="post">
                        @method('PUT')
                        @csrf
                        <br>
                        <div class="input-group mb-3">
                            <select class="form-control{{ $errors->has('state_id') ? ' is-invalid' : '' }}"
                                    id="state_id" name="state_id">
                                <option value="" selected>انتخاب استان</option>
                                @foreach($states as $state)
                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('state_id'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('state_id') }}
                                </div>
                            @endif
                        </div>

                        <div class="input-group mb-3" id="city" style="display:none">

                            <select class="form-control{{ $errors->has('city_id') ? ' is-invalid' : '' }}" id="city_id"
                                    name="city_id">
                                <option value="" selected>Choose city</option>
                            </select>
                            @if($errors->has('city_id'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('city_id') }}
                                </div>
                            @endif
                        </div>

                        <div class="input-group mb-3" id="cityName" style="display:none">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fa fa-building fa-fw"></i>
                            </span>
                            </div>
                            <input type="text" id="city_name" name="city_name"
                                   class="form-control{{ $errors->has('city_name') ? ' is-invalid' : '' }}"
                                   placeholder="City">
                            @if($errors->has('city_name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('city_name') }}
                                </div>
                            @endif
                        </div>

                        <button type="submit" class="form-control btn btn-success">تایید</button>
                    </form>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $('select[name]')
            $('#state_id').change(function () {
                $('#city_id').html('<option value="" selected>Choose city</option>');
                $('#city').show(150);

                var state_id = $(this).val();
                var $city = $('#city_id');

                $.ajax({
                    url: "{{ route('cities')}}",
                    type: 'GET',
                    data: {
                        state_id: state_id
                    },
                    success: function (data) {
                        console.log(data);
                        $city.html('<option value="" selected>Choose city</option>');
                        $.each(data, function (id, value) {
                            $city.append('<option value="' + id + '">' + value + '</option>');
                        });
                        $('#city').show(150);
                    }
                });
            });

        });
    </script>
@endsection

