<nav class="navbar navbar-icon-top navbar-expand-md navbar-dark fixed-top bg-dark d-flex justify-content-center">
    <img src="img/logo.svg" alt="" width="80px">
    <a class="navbar-brand " href="#">کافه ریباز</a>
    <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse collapse justify-content-center" id="navbarCollapse">
        <ul class="nav  navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="{{route('main')}}">صفحه اصلی</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">درباره ما</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">ارتباط با ما</a>
            </li>
        </ul>
    </div>
    <div>
        <ul class="navbar-nav  mt-2 mt-md-0 ">
            <li class="nav-item ">
                <a class="nav-link" href="{{route('cart')}}">
                    <i class="fa fa-shopping-basket">
                        <span
                            class="badge badge-info">{{session()->get('cart')> 0 ? count(session()->get('cart')) : 0}}</span>
                    </i>
                </a>
            </li>
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('fields.login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('fields.register') }}</a>
                    </li>
                @endif
            @else
                <li class="nav-item">
                   <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{Auth::user()->first_name }}<span class="caret"></span>
                    </a>

                    <div class="dropdown-menu " aria-labelledby="navbarDropdown" style="text-align: center ;">

                        @if(Auth::user()->level=='admin')
                            <a class="dropdown-item" href="{{route('dashboard')}}"
                               onclick="event.preventDefault(); document.getElementById('dashboard-form').submit();">
                                 داشبورد
                            </a>
                            <form id="dashboard-form" action="{{ route('dashboard') }}" method="GET" style="display: none;">
                                @csrf
                            </form>
                        @endif


                        <a class="dropdown-item " href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('fields.logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>
    </div>

</nav>

