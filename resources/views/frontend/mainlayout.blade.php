<!DOCTYPE html>
<html lang="en">
<head class="al-hero-header">
    @include('frontend.partials.head')
</head>
<body>
@include('frontend.partials.nav')
@yield('content')
@yield('content1')
@include('frontend.partials.footer')
@include('frontend.partials.footer-scripts')
@yield('scripts')
</body>
</html>
