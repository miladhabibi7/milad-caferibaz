<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{

    public function getOrderDetail(){
              return $this->hasMany(OrderDetail::Class,'order_id');
    }
    public function getUser(){
        return $this->belongsTo(User::Class,'user_id');
    }
    public function getAddress(){
        return $this->belongsTo(Address::Class,'address_id');
    }

}
