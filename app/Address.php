<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public function getUser(){
        return $this->belongsTo(User::Class,'user_id');
    }
    public function getOrder(){
        return $this->hasMany(Order::Class,'address_id');
    }
}
