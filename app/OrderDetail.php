<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    public function getOrder(){
        return $this->belongsTo(Order::Class,'order_id');
        }
}
