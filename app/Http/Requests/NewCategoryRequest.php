<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:50|string',
            'icon' => 'required',//'required|image|dimensions:maxWidth(1000),maxHeight(500),ratio(3 / 2)',
        ];
    }

    public function messages()
    {
        $message = [
            'icon.required' => 'آیکون دسته الزامی است.',
        ];

        return array_merge(parent::messages(), $message);
    }

    public function attributes()
    {
        $m = [
            'title' => __('fields.title'),
            'icon' => __('fields.icon'),
        ];
        return array_merge(parent::attributes(), $m);
    }
}
