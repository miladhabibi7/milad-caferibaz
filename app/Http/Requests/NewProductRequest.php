<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|unique:products|max:50',
            'category_id'=>'required',
            'description'=>'required|max:1000',
            'icon'=>'required',//'required|image|dimensions:maxWidth(1000),maxHeight(500),ratio(3 / 2)',
            'quantity'=>'required|integer',
            'price'=>'required|integer',
            'discount'=>'required|integer',
            'status'=>'required',
        ];
    }
}
