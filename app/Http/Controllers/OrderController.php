<?php


namespace App\Http\Controllers;


use App\Address;
use App\Category;
use App\Order;
use App\OrderDetail;
use App\Product;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;

class OrderController
{
    public function index(){

        $orders=Order::all();
        return view('dashboard.orders.orders', compact('orders'));
    }
    public function store($address_id)
    {
       // dd($address_id);
        $cart = session()->get('cart');
        $order = new Order();
        $order->user_id = Auth::user()->id;
         $order->order_number=$this->generateOrderNumber();
         $order->address_id =$address_id;
        $order->save();



        foreach ($cart as $key => $value){
            $orderDetail=new OrderDetail();
            $orderDetail->order_id=$order->id;
            $orderDetail->product_id=$value["id"];
            $orderDetail->price=$value["price"];
            $orderDetail->quantity=$value["quantity"];
            $orderDetail->save();
        }
        session()->forget('cart');




        return view('frontend.order', compact('order'));
    }

    function generateOrderNumber()
    {
        $number = mt_rand(1000000000, 9999999999); // better than rand()

        // call the same function if the barcode exists already
        if ($this->numberExists($number)) {
            return $this->generateOrderNumber();
        }

        // otherwise, it's valid and can be used
        return $number;
    }

    function numberExists($number)
    {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return Order::where('order_number', $number)->exists();
    }

}
