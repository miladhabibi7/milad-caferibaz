<?php


namespace App\Http\Controllers;


use App\Category;

class OrderDetailsController extends Controller
{

    public function index(){
        $category = Category::query()->orderByDesc('id')->paginate(5);;
        return view('dashboard.orderDetails', compact('category'));
    }
}
