<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewUserRequest;
use App\Product;
use App\State;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('dashboard.users.users', compact('users'));
    }

    public function show(User $user_id)
    {
        return view('dashboard.users.user-edit', compact('user_id'));
    }

    public function create()
    {
        return view('dashboard.users.user-new');
    }

    public function store(NewUserRequest $request)
    {
        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->user_name = $request->user_name;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->level = $request->level;
        $user->status = $request->status;
        $user->save();
        return redirect()->route('users');
    }

    public function update(NewUserRequest $request, User $user_id)
    {
        $user_id->first_name = $request->first_name;
        $user_id->last_name = $request->last_name;
        $user_id->user_name = $request->user_name;
        $user_id->password = $request->password;
        $user_id->email = $request->email;
        $user_id->phone_number = $request->phone_number;
        $user_id->level = $request->level;
        $user_id->status = $request->status;
        $user_id->save();
        return redirect()->route('users');
    }

    public function delete(User $user_id)
    {

        $user_id->delete();

        return redirect()->route('users');

    }

    public function userCity()
    {
        $states = DB::table('states')->get();
        $user_id = User::where('id', Auth::id())->first();
        return view('frontend.select_state_and_city', compact('states', 'user_id'));
    }

    public function stateAndCityUpdate(Request $request, User $user_id)
    {
        $user_id->city_id = $request->city_id;
        $user_id->state_id = $request->state_id;
        $user_id->save();
        return redirect('select_address');
    }
}
