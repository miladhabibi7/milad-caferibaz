<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');


Auth::routes();

Route::get('/', function () {
    return view('welcome');
});


Route::get('select_address', 'AddressController@index')
    ->middleware('selected_city')->name('select_address');;
Route::post('save_address', 'AddressController@store')->name('save_address');;


Route::get('selectCity', 'UsersController@userCity')->name('selectCity');
Route::put('updatecity/{user_id}', 'UsersController@stateAndCityUpdate');
Route::get('cities', 'CityController@index')->name('cities');


Route::prefix('admin')->middleware('admin')->group(function () {

    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    Route::prefix('category')->group(function () {
        Route::get('/', 'CategoryController@index')->name('category.show');
        Route::get('{category_id}', 'CategoryController@show')->name('category.edit');
        Route::get('/create/new', 'CategoryController@create')->name('category.create');
        Route::post('/', 'CategoryController@store');
        Route::put('{category_id}', 'CategoryController@update');
        Route::delete('{category_id}', 'CategoryController@delete')->name('category.delete');
    });

    Route::prefix('products')->group(function () {
        Route::get('/', 'ProductsController@index')->name('products.show');
        Route::get('{product_id}', 'ProductsController@show')->name('product.edit');
        Route::get('/create/new', 'ProductsController@create')->name('product.create');
        Route::post('/', 'ProductsController@store');
        Route::put('{product_id}', 'ProductsController@update');
        Route::delete('{product_id}', 'ProductsController@delete')->name('product.delete');
    });

    Route::prefix('users')->group(function () {
        Route::get('/', 'usersController@index')->name('users');
        Route::get('{user_id}', 'usersController@show')->name('user.edit');
        Route::get('/create/new', 'UsersController@create')->name('user.create');
        Route::post('/', 'usersController@store');
        Route::put('{user_id}', 'usersController@update')->name('user.update');
        Route::delete('{user_id}', 'usersController@delete')->name('user.delete');
    });

    Route::get('orders', 'OrderController@index')->name('orders.list');



});


Route::prefix('main')->group(function () {
    Route::get('/', 'front\MainPageController@index')->name('main');
    Route::get('productList/{category_id}', 'front\ProductListPageController@index')->name('product.list');
    Route::get('productDetails/{product}', 'front\ProductDetailsController@index')->name('product.details');

});


Route::get('cart', 'front\CartPageController@index')->name('cart');
Route::get('{id}', 'front\CartPageController@delete')->name('cart.delete');


Route::get('add_to_cart/{id}', 'ProductsController@addToCart')->name('product.add_to_cart');
Route::get('save_order/{address_id}', 'OrderController@store')->name('order');

Route::get('order_details', 'OrderDetailsController@index')->name('order_details');




